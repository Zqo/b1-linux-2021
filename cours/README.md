# Cours

## Cours

- [Introduction à Linux](./intro/README.md)
  - Relation entre le matériel, le noyau, l'OS, le shell et l'utilisateur
  - Linux
  - GNU
  - Libre et Open-Source

## Notions

Mini-cours sur des notions précises.

- [Permissions POSIX (`rwx`)](./notions/permissions.md)

## Memos

- [Mémo commandes GNU/Linux](./memos/commandes.md)