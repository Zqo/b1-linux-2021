# TP1 : Are you dead yet ?

Ce premier TP a pour objectif de vous familiariser avec les environnements GNU/Linux.  

On va apprendre à quoi servent les commandes élémentaires comme `cat`, `cd`, `ls`... **non c'est pas vrai, on va casser des machines d'abord. Hihi.**

---

**Munissez vous de :**

- votre créativité
- votre meilleur moteur de recherche
- une machine virtuelle GNU/Linux
  - p'tit snapshot avant de tout péter !
- y'aura sûrement besoin du terminal, plus simple de tout péter depuis là :)

---

**Le but va être de péter la machine virtuelle.**

Peu importe comment, faut la péteeeeer.

Par "péter" on entend la rendre inutilisable. Ca peut être strictement inutilisable, ou des trucs un peu plus légers, **soyez créatifs**.

➜ Si la machine boot même plus, c'est valide.  
➜ Si la machine boot mais qu'on peut rien faire du tout, c'est valide.  
➜ Si la machine boot, qu'on peut faire des trucs mais que pendant 15 secondes après c'est mort, c'est valide.  
➜ Si ça boot étou, mais que c'est VRAIMENT CHIANT d'utiliser la machine, c'est VALIDE.  

**Bref si on peut pas utiliser la machine normalement, c'est VA-LI-DE.**  

![ARE U DED YET](./pics/dead-yet.gif)

---

**Des idées en vrac :**

➜ pensez à comment **surcharger** la machine, d'un point de vue matériel  
➜ des trucs **vicieux** qui font rien au début, et qui pète le système petit à petit, c'est rigolo hihi  
➜ si ça peut vous donner des idées : parmi **les principaux composants d'un système GNU/Linux** (et d'un OS de façon générale), on a...

- un *filesystem* ou *système de fichiers*
- des *utilisateurs* et des *permissions*
- des *processus*
- une *stack réseau* (genre des cartes réseau, avec des IP dessus, toussa)
- un *shell* pour que les humains puissent utiliser la machine
  - que ce soit une interface graphique (GUI) ou un terminal (CLI)
- des *devices* ou *périphériques*
  - écran, clavier, souris, disques durs, etc.

> **Essayez de penser par vous-mêmes, de raisonner.** Et pas direct Google "how to break a linux machine" comme des idiots. *(quand je dis de pas faire un truc, il faut le faire, c'est genre la règle n°1. Mais réfléchissez un peu quand même avant de Google ça)*

---

🌞 **Trouver au moins 5 façons différentes de péter la machine**

- elles doivent être **vraiment différentes**
- je veux le procédé exact utilisé
  - généralement une commande ou une suite de commandes
- il faut m'expliquer avec des mots comment ça marche
  - pour chaque méthode utilisée, me faut l'explication avec
  - et ui va falloir comprendre ce que vous faites n_n

➜ Vous aurez une note acceptable si vous me donnez 5 façons différentes de péter la machine.  
➜ Si vous m'en fournissez plus que 5, la note grimpe.  
➜ Plus c'est original, voire absurde, plus ça grimpe. Privilégiez-donc la qualité à la quantité, de loin.  
➜ Plus c'est fait à la main, par vos ptites mains, et votre ptite tête, plus ça grimpe.

> Je vous ferai un p'tit top 5 au prochain cours.

![Boom](pics/grumpy-cat-explode.gif)