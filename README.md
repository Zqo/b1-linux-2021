# B1 Linux 2021

Ici seront déposés tous les supports de cours liés au cours Linux 1ère année 2021.

### ➜ [**Cours**](./cours/README.md)

- [Introduction à Linux](./cours/intro/README.md)

#### ➜ [**Notions**](./tp/README.md)

- [Permissions POSIX](./cours/notions/permissions.md)

#### ➜ [**Memo**](./tp/README.md)

- [Mémo commandes Linux](./cours/memos/commandes.md)

### ➜ [**TP**](./tp/README.md)

- [TP1 : Are you dead yet ?](tp/1/README.md)